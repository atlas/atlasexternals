# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of AnalysisBaseExternals.
#
+ External/HDF5
+ External/BAT
+ External/Boost
+ External/Davix
+ External/dcap
+ External/Eigen
+ External/lwtnn
+ External/FastJet
+ External/FastJetContrib
+ External/GoogleTest
+ External/KLFitter
+ External/Lhapdf
+ External/LibXml2
+ External/onnxruntime
+ External/nlohmann_json
+ External/PyAnalysis
+ External/PyModules
+ External/Python
+ External/ROOT
+ External/SQLite
+ External/TBB
+ External/XRootD
- .*
