# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Main configuration file for building the TestExternals project.
#

# Set up the project.
cmake_minimum_required( VERSION 3.25 )
project( TestExternals VERSION 22.0.0 LANGUAGES CXX )

# Set where to pick up AtlasCMake and AtlasLCG from.
set( AtlasCMake_DIR "${CMAKE_SOURCE_DIR}/../../Build/AtlasCMake"
   CACHE PATH "Location of AtlasCMake" )
set( LCG_DIR "${CMAKE_SOURCE_DIR}/../../Build/AtlasLCG"
   CACHE PATH "Location of AtlasLCG" )
mark_as_advanced( AtlasCMake_DIR LCG_DIR )

# Find the ATLAS CMake code:
find_package( AtlasCMake REQUIRED )

# Set up the AtlasLCG modules, without setting up an actual LCG release.
# This is necessary to have the AtlasLCG code installed together with the
# project. We don't actually need the LCG code for the test project's build.
set( LCG_VERSION_POSTFIX ""
   CACHE STRING "Post-fix for the LCG version number" )
set( LCG_VERSION_NUMBER 0
   CACHE STRING "LCG version number to use for the project" )
find_package( LCG ${LCG_VERSION_NUMBER} REQUIRED )

# Set up CTest:
atlas_ctest_setup()

# Declare project name:
atlas_project( PROJECT_ROOT ${CMAKE_SOURCE_DIR}/../../ )

# Install the export sanitizer script:
install( FILES ${CMAKE_SOURCE_DIR}/atlas_export_sanitizer.cmake.in
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules/skeletons )

# Generate the environment setup for the externals. No replacements need to be
# done to it, so it can be used for both the build and the installed release.
lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh )
install( FILES ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh
   DESTINATION . )

# Package up the release using CPack:
atlas_cpack_setup()
