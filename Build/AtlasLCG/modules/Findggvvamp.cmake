# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# - Locate ggvvamp library
# Defines:
#
#  GGVVAMP_FOUND
#  GGVVAMP_INCLUDE_DIR
#  GGVVAMP_INCLUDE_DIRS
#  GGVVAMP_<component>_LIBRARY
#  GGVVAMP_<component>_FOUND
#  GGVVAMP_LIBRARIES
#  GGVVAMP_LIBRARY_DIRS
#
# Can be steered by GGVVAMP_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# First just find the headers:
lcg_external_module( NAME ggvvamp
  INCLUDE_SUFFIXES include INCLUDE_NAMES ggvvamp.h 
  LIBRARY_SUFFIXES lib lib64
  DEFAULT_COMPONENTS ggvvamp)

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( ggvvamp DEFAULT_MSG
  GGVVAMP_INCLUDE_DIR GGVVAMP_LIBRARIES )
mark_as_advanced( GGVVAMP_FOUND GGVVAMP_INCLUDE_DIR GGVVAMP_INCLUDE_DIRS
  GGVVAMP_LIBRARIES GGVVAMP_LIBRARY_DIRS )

# Set up the RPM dependency:
lcg_need_rpm( ggvvamp )
