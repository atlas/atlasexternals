# Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
#
# Module finding RECOLA in the LCG release. Defines:
#  - RECOLA_FOUND
#  - RECOLA_INCLUDE_DIR
#  - RECOLA_INCLUDE_DIRS
#  - RECOLA_LIBRARIES
#  - RECOLA_LIBRARY_DIRS
#
# Can be steered by RECOLA_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# Declare the external module:
lcg_external_module( NAME recola
   INCLUDE_SUFFIXES include INCLUDE_NAMES recola.h
   LIBRARY_SUFFIXES lib lib64
   COMPULSORY_COMPONENTS recola )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( Recola DEFAULT_MSG RECOLA_INCLUDE_DIR
   RECOLA_LIBRARIES )
mark_as_advanced( RECOLA_FOUND RECOLA_INCLUDE_DIR RECOLA_INCLUDE_DIRS
   RECOLA_LIBRARIES RECOLA_LIBRARY_DIRS )

# Set up the RPM dependency:
lcg_need_rpm( recola )
