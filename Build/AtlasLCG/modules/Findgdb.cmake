# Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
#
# Locate gdb exectuable
#
# Sets:
#  GDB_FOUND
#  GDB_INCLUDE_DIR
#  GDB_INCLUDE_DIRS
#  GDB_BINARY_PATH
#  GDB_gdb_EXECUTABLE
#
# Can be steered by GDB_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# Set up a dependency on Python:
find_package( Python COMPONENTS Interpreter Development )

# Declare the external module:
lcg_external_module( NAME gdb
   INCLUDE_SUFFIXES include
   INCLUDE_NAMES bfd.h
   LIBRARY_SUFFIXES lib lib64
   DEFAULT_COMPONENTS bfd
   BINARY_SUFFIXES bin
   BINARY_NAMES gdb )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( gdb
   REQUIRED_VARS GDB_gdb_EXECUTABLE GDB_INCLUDE_DIR )

# Set up the RPM dependency.
lcg_need_rpm( gdb )
