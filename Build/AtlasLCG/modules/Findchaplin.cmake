# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# - Locate chaplin library
# Defines:
#
#  CHAPLIN_FOUND
#  CHAPLIN_<component>_LIBRARY
#  CHAPLIN_<component>_FOUND
#  CHAPLIN_LIBRARIES
#  CHAPLIN_LIBRARY_DIRS
#
# Can be steered by CHAPLIN_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# First just find the headers:
lcg_external_module( NAME chaplin
  LIBRARY_SUFFIXES lib lib64
  COMPULSORY_COMPONENTS chaplin )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( chaplin DEFAULT_MSG
  CHAPLIN_LIBRARIES )
mark_as_advanced( CHAPLIN_FOUND 
  CHAPLIN_LIBRARIES CHAPLIN_LIBRARY_DIRS )

# Set up the RPM dependency:
lcg_need_rpm( chaplin )
