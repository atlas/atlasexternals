# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Module finding NLOX in the LCG release. Defines:
#  - NLOX_FOUND
#  - NLOX_LIBRARIES
#  - NLOX_LIBRARY_DIR
#  - NLOX_LIBRARY_DIRS
#
# Can be steered by NLOX_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# Declare the external module:
lcg_external_module( NAME nlox
   LIBRARY_SUFFIXES lib lib64
   COMPULSORY_COMPONENTS tred )

# Find the library directory.
find_library( _NLOX_tred_LIBRARY
   NAMES tred
   PATHS "${NLOX_LCGROOT}"
   PATH_SUFFIXES lib lib64 )
get_filename_component( NLOX_LIBRARY_DIR
   "${_NLOX_tred_LIBRARY}" PATH )
mark_as_advanced( _NLOX_tred_LIBRARY )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( nlox DEFAULT_MSG
   NLOX_LIBRARY_DIR NLOX_LIBRARIES NLOX_LIBRARY_DIRS )
mark_as_advanced( NLOX_FOUND
   NLOX_LIBRARIES NLOX_LIBRARY_DIRS )

# Set up the NLOX_PATH environment variable.
if( NLOX_FOUND )
   set( NLOX_ENVIRONMENT
      SET NLOX_PATH "${NLOX_LIBRARY_DIR}" )
endif()

# Set up the RPM dependency:
lcg_need_rpm( nlox )
