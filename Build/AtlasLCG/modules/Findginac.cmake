# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# - Locate ginac library
# Defines:
#
#  GINAC_FOUND
#  GINAC_INCLUDE_DIR
#  GINAC_INCLUDE_DIRS
#  GINAC_<component>_LIBRARY
#  GINAC_<component>_FOUND
#  GINAC_LIBRARIES
#  GINAC_LIBRARY_DIRS
#
# Can be steered by GINAC_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# First just find the headers:
lcg_external_module( NAME ginac
  INCLUDE_SUFFIXES include INCLUDE_NAMES ginac/ginac.h
  LIBRARY_SUFFIXES lib lib64
  DEFAULT_COMPONENTS ginac)

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( ginac DEFAULT_MSG
  GINAC_INCLUDE_DIR GINAC_LIBRARIES )
mark_as_advanced( GINAC_FOUND GINAC_INCLUDE_DIR GINAC_INCLUDE_DIRS
  GINAC_LIBRARIES GINAC_LIBRARY_DIRS )

# Set up the RPM dependency:
lcg_need_rpm( ginac )
