# Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
#
# This file is here to intercept find_package(Boost) calls, and
# massage the paths produced by the system module, to make them relocatable.
#

# The LCG include(s):
include( LCGFunctions )

# Construct the library names of the components that were requested.
set( _boost_components )
foreach( _comp ${Boost_FIND_COMPONENTS} )
   if( "${_comp}" STREQUAL "ALL" )
      list( APPEND _boost_components Boost::filesystem )
   else()
      list( APPEND _boost_components Boost::${_comp} )
   endif()
endforeach()
list( REMOVE_DUPLICATES _boost_components )

# Use the helper macro to do most of the work:
lcg_wrap_find_module( Boost
   NO_LIBRARY_DIRS
   CONFIG
   IMPORTED_TARGETS ${_boost_components} )
unset( _boost_components )

# Cache the library directories, if there are any. So that future calls to
# this module, which do not ask for shared libraries, would not erase the
# already found directory.
if( Boost_LIBRARY_DIRS )
   set( Boost_LIBRARY_DIRS ${Boost_LIBRARY_DIRS} CACHE INTERNAL
      "Cached Boost library directory" )
endif()

# We just use this to print a one-time "Found" message:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( Boost
   FOUND_VAR Boost_FOUND
   REQUIRED_VARS Boost_DIR Boost_FOUND
   VERSION_VAR Boost_VERSION )

# Set up the RPM dependency:
lcg_need_rpm( Boost )
