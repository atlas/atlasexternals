# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Sets:
#  COVERAGE_PYTHON_PATH
#  COVERAGE_BINARY_PATH
#  COVERAGE_coverage_EXECUTABLE
#
# Can be steered by COVERAGE_LCGROOT.
#

# The LCG include(s).
include( LCGFunctions )

# Find it.
lcg_python_external_module( NAME coverage
   PYTHON_NAMES coverage/__init__.py coverage.py
   BINARY_NAMES coverage
   BINARY_SUFFIXES bin )

# Handle the standard find_package arguments.
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( coverage DEFAULT_MSG
   _COVERAGE_PYTHON_PATH _COVERAGE_BINARY_PATH )

# Set up the RPM dependency.
lcg_need_rpm( coverage )
