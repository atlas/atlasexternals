GDB: The GNU Project Debugger
=============================

This package can be used to build `gdb` in case it is not taken from LCG, which
is the default. To enable this standalone build:

   * add `External/Gdb` to the project's `package_filters.txt`
   * remove the `find_pacakge( gdb )` from the project's `CMakeLists.txt`
