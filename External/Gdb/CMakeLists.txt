# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Package building GDB as part of the offline software build.
#

# The name of the package:
atlas_subdir( Gdb )

# Set up a dependency on Python:
find_package( Python COMPONENTS Interpreter Development REQUIRED )
find_package( ZLIB )
find_package( mpfr )
find_package( xz )

# Declare where to get GDB from.
set( ATLAS_GDB_SOURCE
   "URL;https://cern.ch/atlas-software-dist-eos/externals/Gdb/gdb-16.1.tar.gz;URL_MD5;52324289d7330f84cd5fd577408bc7c2"
   CACHE STRING "The source for GDB" )
mark_as_advanced( ATLAS_GDB_SOURCE )

# Decide whether / how to patch the GDB sources.
set( ATLAS_GDB_PATCH
   "PATCH_COMMAND;patch;-p0;-i;${CMAKE_CURRENT_SOURCE_DIR}/patches/gdb-8.0.patch;COMMAND;patch;-p1;-i;${CMAKE_CURRENT_SOURCE_DIR}/patches/gdb-disable-makeinfo.patch"
   CACHE STRING "Patch command for GDB" )
set( ATLAS_GDB_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of GDB (2022.10.11.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_GDB_PATCH ATLAS_GDB_FORCEDOWNLOAD_MESSAGE )

# Temporary build directory:
set( _buildDir "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/GdbBuild" )

# Look for makeinfo. As if it's not available, we need to tell the build
# configuration about it.
find_program( _makeinfoExe NAMES makeinfo HINTS ENV PATH )
set( _makeinfoSetting "MAKEINFO=true" )
if( _makeinfoExe )
   set( _makeinfoSetting "MAKEINFO=${_makeinfoExe}" )
endif()

# Create the script that will configure the build of Gdb.
if( ("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang") AND ("${CMAKE_CXX_COMPILER_VERSION}" VERSION_GREATER_EQUAL "16") )
  set( _clang_flags "-Wno-error=enum-constexpr-conversion" )
endif()
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/cmake/configure.sh.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh" @ONLY )

# Libraries to link against:
set( _libraries
  "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/${CMAKE_STATIC_LIBRARY_PREFIX}bfd${CMAKE_STATIC_LIBRARY_SUFFIX}" )
list( APPEND _libraries
  "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/${CMAKE_STATIC_LIBRARY_PREFIX}iberty${CMAKE_STATIC_LIBRARY_SUFFIX}" )
list( APPEND _libraries
  "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/${CMAKE_STATIC_LIBRARY_PREFIX}sframe${CMAKE_STATIC_LIBRARY_SUFFIX}" )

# Set up the build of GDB for the build area:
ExternalProject_Add( Gdb
   PREFIX "${CMAKE_BINARY_DIR}"
   ${ATLAS_GDB_SOURCE}
   ${ATLAS_GDB_PATCH}
   DOWNLOAD_EXTRACT_TIMESTAMP TRUE  # keep original timestamps (ATLINFR-4765)
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   CONFIGURE_COMMAND
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh"
   INSTALL_COMMAND make install
   COMMAND ${CMAKE_COMMAND} -E copy_directory
   "${_buildDir}/" "<INSTALL_DIR>"
   BUILD_BYPRODUCTS ${_libraries} )
ExternalProject_Add_Step( Gdb forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_GDB_FORCEDOWNLOAD_MESSAGE}"
   INDEPENDENT TRUE
   DEPENDERS download )
ExternalProject_Add_Step( Gdb purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for Gdb"
   INDEPENDENT TRUE
   DEPENDEES download
   DEPENDERS patch )
add_dependencies( Package_Gdb Gdb )

# Set up its installation:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install its find-module:
install( FILES "cmake/Findgdb.cmake"
   DESTINATION "${CMAKE_INSTALL_CMAKEDIR}/modules" )

# Build the package's application:
atlas_add_executable( resolveAtlasAddr2Line src/resolveAtlasAddr2Line.cxx
   INCLUDE_DIRS ${CMAKE_INCLUDE_OUTPUT_DIRECTORY} ${ZLIB_INCLUDE_DIRS}
   LINK_LIBRARIES ${_libraries} ${ZLIB_LIBRARIES} ${CMAKE_DL_LIBS} zstd )
# Required macro usually set by autotools (https://stackoverflow.com/q/11748035):
target_compile_definitions( resolveAtlasAddr2Line PRIVATE
   PACKAGE=Gdb )
add_dependencies( resolveAtlasAddr2Line Gdb )

# Install its other resources:
atlas_install_python_modules( python/__init__.py python/gdbhacks )
atlas_install_scripts( scripts/atlasAddress2Line )
