# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Locate the APTypes external package.
#
# Defines:
#  APTYPES_FOUND
#  APTYPES_INCLUDE_DIR
#  APTYPES_INCLUDE_DIRS
#

# Include the helper code:
include( AtlasInternals )

# Declare the module:
atlas_external_module( NAME APTypes
   INCLUDE_SUFFIXES include/ap_types
   INCLUDE_NAMES ap_int.h )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( APTypes DEFAULT_MSG APTYPES_INCLUDE_DIRS )
mark_as_advanced( APTYPES_FOUND APTYPES_INCLUDE_DIR APTYPES_INCLUDE_DIRS )
