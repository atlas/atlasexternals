APTypes
=======

This package downloads and installs the Xilinx HLS arbitrary precision types
from https://github.com/Xilinx/HLS_arbitrary_Precision_Types.

To use the types in your own code, add the following to your `CMakeLists.txt`:

```cmake
find_package( APTypes )

atlas_add_library( MyLib
   src/*.cxx
   INCLUDE_DIRS ${APTYPES_INCLUDE_DIRS}
   ... )
```

and include the headers via:
```c++
#include "ap_int.h"
```
