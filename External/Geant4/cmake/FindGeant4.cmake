# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# Locate Geant4.
# Defines:
#  - GEANT4_FOUND
#  - GEANT4_INCLUDE_DIR
#  - GEANT4_INCLUDE_DIRS
#  - GEANT4_<component>_FOUND
#  - GEANT4_<component>_LIBRARY
#  - GEANT4_LIBRARIES
#  - GEANT4_LIBRARY_DIRS
#
# Can be steered by GEANT4_HOME_DIR + GEANT4_VERSION.
#

# Save and restore global settings changed by Geant's find script
get_directory_property(_include_dirs INCLUDE_DIRECTORIES)

find_package(Geant4 QUIET CONFIG)
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Geant4 CONFIG_MODE)

if(Geant4_FOUND)
  # Geant4 calls `include_directories` for CLHEP :( which is not what we want!
  # Save and restore include directories around the call
  set_directory_properties(PROPERTIES INCLUDE_DIRECTORIES "${_include_dirs}")
endif()
unset(_include_dirs)

# ATLAS code uses variables prefixed "GEANT4" rather than "Geant4", so we must
# provide translations
set(GEANT4_FOUND ${Geant4_FOUND})
set(GEANT4_INCLUDE_DIR ${Geant4_INCLUDE_DIR})
set(GEANT4_INCLUDE_DIRS ${Geant4_INCLUDE_DIRS})
set(GEANT4_LIBRARIES ${Geant4_LIBRARIES})

# Filter out Geant4 components we don't need in Athena **unless** the COMPONENTS argument to
# find_package was supplied by the client
if( "${Geant4_FIND_COMPONENTS}" STREQUAL "" )
   list(FILTER GEANT4_LIBRARIES EXCLUDE REGEX "Geant4::G4(FR|GMocren|RayTracer|VRML|analysis|error_propagation|parmodels)")
endif()

# Try to extract the version of the found G4 instance:
# We have to do this because the ATLAS build changes this to account for ATLAS patches
# and this version is used to point to the data files
if( GEANT4_INCLUDE_DIR )
   file( READ "${GEANT4_INCLUDE_DIR}/G4Version.hh" _g4version_h
      LIMIT 20000 )
   set( _version_regex
      ".*# *define G4VERSION_TAG \".Name. ([^\"]*) .\".*" )
   if( "${_g4version_h}" MATCHES ${_version_regex} )
      set( G4_VERSION ${CMAKE_MATCH_1} CACHE INTERNAL
         "Detected version of Geant4" )
   else()
      set( G4_VERSION "${GEANT4_VERSION}" CACHE INTERNAL
         "Specified version of Geant4" )
   endif()
else()
   set( G4_VERSION "${GEANT4_VERSION}" CACHE INTERNAL
      "Specified version of Geant4" )
endif()


# If Geant4 was found, set up the data environment variables for it
# NB: Once Athena requires v11.1 or newer, only one data environment variable
# will be needed
if( GEANT4_FOUND )

   # Default location of the data files. To be overridden by AtlasSetup:
   set( _g4pathDefault "/eos/atlas/atlascerngroupdisk/proj-geant4/releases" )
   if( NOT "$ENV{G4PATH}" STREQUAL "" )
      set( _g4pathDefault "$ENV{G4PATH}" )
   endif()
   set( G4PATH "${_g4pathDefault}" CACHE PATH "Location of the G4 data files" )
   mark_as_advanced( G4PATH )
   # Location of the data files:
   set( _g4data "\${G4PATH}/share/\${G4VERS}/data" )

   # Set the environment:
   set( GEANT4_ENVIRONMENT
      SET G4PATH "${G4PATH}"
      SET G4VERS "${G4_VERSION}"
      SET G4NEUTRONHPDATA   "${_g4data}/G4NDL"
      SET G4LEDATA          "${_g4data}/G4EMLOW"
      SET G4LEVELGAMMADATA  "${_g4data}/PhotonEvaporation"
      SET G4RADIOACTIVEDATA "${_g4data}/RadioactiveDecay"
      SET G4PARTICLEXSDATA  "${_g4data}/G4PARTICLEXS"
      SET G4PIIDATA         "${_g4data}/G4PII"
      SET G4REALSURFACEDATA "${_g4data}/RealSurface"
      SET G4SAIDXSDATA      "${_g4data}/G4SAIDDATA"
      SET G4ABLADATA        "${_g4data}/G4ABLA"
      SET G4INCLDATA        "${_g4data}/G4INCL"
      SET G4ENSDFSTATEDATA  "${_g4data}/G4ENSDFSTATE" )

   # Set up a dependency on the G4 data RPM:
   set_property( GLOBAL APPEND PROPERTY ATLAS_EXTERNAL_RPMS
      "ATLAS_Geant4Data_${G4_VERSION}" )

   # Clean up:
   unset( _g4data )
   unset( _g4pathDefault )
endif()
