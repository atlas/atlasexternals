# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building/configuring prmon as part of the offline
# software build.
#

# Set the name of the package:
atlas_subdir( prmon )

# Declare where to get prmon from.
set( ATLAS_PRMON_SOURCE
   "URL;https://atlas-software-dist-eos.web.cern.ch/externals/prmon/v3.1.1.tar.gz;URL_MD5;3131b07daa91b478163a68888bd8d706"
   CACHE STRING "The source for prmon" )
mark_as_advanced( ATLAS_PRMON_SOURCE )

# Decide whether / how to patch the prmon sources.
set( ATLAS_PRMON_PATCH "" CACHE STRING "Patch command for prmon" )
set( ATLAS_PRMON_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of prmon (2022.10.11.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_PRMON_PATCH ATLAS_PRMON_FORCEDOWNLOAD_MESSAGE )

# Print what's going on
message( STATUS "Buliding prmon as part of this project" )

# External dependencies
if( NOT ATLAS_BUILD_NLOHMANN_JSON )
   find_package( nlohmann_json )
endif()

# Temporary directory for the build results
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/prmonBuild" )

# List of paths given to CMAKE_PREFIX_PATH.
set( _prefixPaths "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   $ENV{CMAKE_PREFIX_PATH} ${nlohmann_json_DIR} )

# Extras
set( _extraArgs )
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()
if( NOT "${CMAKE_CXX_STANDARD}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()

# Build prmon for the build area:
ExternalProject_Add( prmon
  PREFIX "${CMAKE_BINARY_DIR}"
  INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
  ${ATLAS_PRMON_SOURCE}
  ${ATLAS_PRMON_PATCH}
  CMAKE_CACHE_ARGS
  -DCMAKE_PREFIX_PATH:PATH=${_prefixPaths}
  -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
  -DCMAKE_INSTALL_BINDIR:PATH=${CMAKE_INSTALL_BINDIR}
  -DCMAKE_INSTALL_LIBDIR:PATH=${CMAKE_INSTALL_LIBDIR}
   ${_extraArgs}
  LOG_CONFIGURE 1 )
ExternalProject_Add_Step( prmon forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_PRMON_FORCEDOWNLOAD_MESSAGE}"
   INDEPENDENT TRUE
   DEPENDERS download )
ExternalProject_Add_Step( prmon purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for prmon."
   INDEPENDENT TRUE
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( prmon forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f "<BINARY_DIR>/CMakeCache.txt"
   COMMENT "Forcing the configuration of prmon"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( prmon buildinstall
  COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
  COMMENT "Installing prmon into the build area"
  DEPENDEES install )
if( ATLAS_BUILD_NLOHMANN_JSON )
   add_dependencies( prmon nlohmann_json )
endif()
add_dependencies( Package_prmon prmon )

# Install it here
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
