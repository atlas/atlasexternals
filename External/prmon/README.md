prmon
=======

This package builds Process Monitor (prmon), a 
small stand alone program that can monitor the 
resource consumption of a process and its children.
See https://github.com/HSF/prmon for more information.
