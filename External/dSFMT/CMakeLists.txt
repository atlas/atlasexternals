# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# Package building the dSFMT libraries as part of the ATLAS offline software
# build.
#

# The name of the package.
atlas_subdir( dSFMT )

# Declare where to get dSFMT from.
set( ATLAS_DSFMT_SOURCE
   "URL;${CMAKE_CURRENT_SOURCE_DIR}/src/dSFMT-src-2.1.tar.gz;URL_MD5;b3a38dac7fd8996a70d02edc4432dd75"
   CACHE STRING "The source for dSFMT" )
mark_as_advanced( ATLAS_DSFMT_SOURCE )

# Decide whether / how to patch the dSFMT sources.
set( ATLAS_DSFMT_PATCH "" CACHE STRING "Patch command for dSFMT" )
set( ATLAS_DSFMT_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of dSFMT (2022.10.11.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_DSFMT_PATCH ATLAS_DSFMT_FORCEDOWNLOAD_MESSAGE )

# Directory to put the intermediate build results in.
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/dSFMTBuild" )

# Generate the Makefile used for the build.
set( SSE2FLAGS )
if( "${CMAKE_SYSTEM_PROCESSOR}" STREQUAL "x86_64" )
   set( SSE2FLAGS "-msse2 -DHAVE_SSE2" )
endif()
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/src/Makefile.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/Makefile" @ONLY )

# Build the package for the build area.
ExternalProject_Add( dSFMT
   PREFIX "${CMAKE_BINARY_DIR}"
   ${ATLAS_DSFMT_SOURCE}
   ${ATLAS_DSFMT_PATCH}
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   PATCH_COMMAND ${CMAKE_COMMAND} -E copy
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/Makefile"
   "<SOURCE_DIR>/Makefile"
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E echo
   "Configuring the build of dSFMT"
   INSTALL_COMMAND make install
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   BUILD_IN_SOURCE 1 )
ExternalProject_Add_Step( dSFMT forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_DSFMT_FORCEDOWNLOAD_MESSAGE}"
   INDEPENDENT TRUE
   DEPENDERS download )
add_dependencies( Package_dSFMT dSFMT )

# Install it.
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install its find-module.
install( FILES "cmake/FinddSFMT.cmake"
   DESTINATION "${CMAKE_INSTALL_CMAKEDIR}/modules" )
