/*
 * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file CheckerGccPlugins/src/feshim.cxx
 * @author scott snyder <snyder@bnl.gov>
 * @date Apr, 2024
 * @brief Shims for accessing front-end symbols.
 */

#define FESHIM_CXX
#define scope_chain dum_scope_chain
#include "checker_gccplugins.h"
#include "tree.h"
#include "cp/cp-tree.h"
#include "feshim.h"
#include <dlfcn.h>
#undef scope_chain

// Shim variables.
tree cp_global_trees[CPTI_MAX];
cpp_reader* parse_in = nullptr;


namespace CheckerGccPlugins
{


//********************************************************************
// The shim functions.
// These will bounce the calls to the real_* pointers which
// we retrieved in init_shims().


typedef void (*c_register_pragma_t) (const char* space,
                                     const char* type,
                                     pragma_handler_1arg handler);
c_register_pragma_t real_c_register_pragma = nullptr;


void c_register_pragma (const char *space, const char *name,
                        pragma_handler_1arg handler)
{
  (*real_c_register_pragma) (space, name, handler);
}


//********


typedef void (*cpp_define_t) (cpp_reader *, const char *);
cpp_define_t real_cpp_define = nullptr;
void cpp_define (const char* def)
{
  (*real_cpp_define) (parse_in, def);
}


//********


typedef tree (*strip_typedefs_t) (tree t, bool* remove_attributes, unsigned int flags);
strip_typedefs_t real_strip_typedefs = nullptr;
tree strip_typedefs (tree t, bool* remove_attributes, unsigned int flags)
{
  return (*real_strip_typedefs) (t, remove_attributes, flags);
}


//********


typedef tree (*lookup_base_t) (tree t, tree base, /*base_access*/int access,
                               /*base_kind*/int *kind_ptr,
                               /*tsubst_flags_t*/int complain
#if GCC_VERSION >= 14000
                               , HOST_WIDE_INT offset
#endif
                               );
lookup_base_t real_lookup_base = nullptr;
tree lookup_base (tree t, tree base)
{
  return (*real_lookup_base) (t, base,
                              0, // ba_any
                              NULL,
                              0 // tf_none
#if GCC_VERSION >= 14000
                              , -1
#endif
                              );
}


//********


typedef const char* (*type_as_string_t) (tree t, int flags);
type_as_string_t real_type_as_string = nullptr;
const char* type_as_string (tree t, int flags)
{
  return (*real_type_as_string) (t, flags);
}


//********


typedef const char* (*decl_as_string_t) (tree t, int flags);
decl_as_string_t real_decl_as_string = nullptr;
const char* decl_as_string (tree t, int flags)
{
  return (*real_decl_as_string) (t, flags);
}


//********


typedef tree (*skip_artificial_parms_for_t) (const_tree fn, tree list);
skip_artificial_parms_for_t real_skip_artificial_parms_for = nullptr;
tree skip_artificial_parms_for (const_tree fn, tree list)
{
  return (*real_skip_artificial_parms_for) (fn, list);
}


//********


typedef tree (*dfs_walk_all_t) (tree binfo, tree (*pre_fn) (tree, void *),
                                tree (*post_fn) (tree, void *), void *data);
dfs_walk_all_t real_dfs_walk_all = nullptr;
tree
dfs_walk_all (tree binfo, tree (*pre_fn) (tree, void *),
              tree (*post_fn) (tree, void *), void *data)
{
  return (*real_dfs_walk_all) (binfo, pre_fn, post_fn, data);
}


//********


typedef int (*cp_type_quals_t) (const_tree t);
cp_type_quals_t real_cp_type_quals = nullptr;
int cp_type_quals (const_tree t)
{
  return (*real_cp_type_quals) (t);
}


//********


typedef tree (*look_for_overrides_here_t) (tree type, tree fndecl);
look_for_overrides_here_t real_look_for_overrides_here = nullptr;
tree look_for_overrides_here (tree type, tree fndecl)
{
  return (*real_look_for_overrides_here) (type, fndecl);
}


//********************************************************************
// Shim for getting the current namespace.


saved_scope** scope_chain_ptr = nullptr;
tree get_current_namespace()
{
  return (*scope_chain_ptr)->old_namespace;
}


//********************************************************************


bool init_shims()
{
  // Handle referencing the main program.
  void* handle = dlopen (NULL, 0);

  // Check to see if any FE symbols are present.
  // If not, exit gracefully and disable the checkers.
  if (!dlsym (handle, "scope_chain")) {
    return false;
  }

  // Helper for looking up a symbol.
  // Abort if we don't find the symbol.
  auto findsym = [&] (auto& fn, const char* sym)
  {
    void* p = dlsym (handle, sym);
    if (!p) {
      fatal_error (input_location, "CheckerGccPlugins: cannot find symbol %<%s%>", sym);
    }
    fn = (std::remove_reference_t<decltype(fn)>)p;
  };

  // Look up this symbol and copy it to our local array.
  tree* trees;
  findsym (trees, "cp_global_trees");
  memcpy (cp_global_trees, trees, sizeof (cp_global_trees));

  // Variable pointers.
  
  findsym (scope_chain_ptr, "scope_chain");

  cpp_reader** parse_in_ptr = nullptr;
  findsym (parse_in_ptr, "parse_in");
  parse_in = *parse_in_ptr;


  // Function pointers.

  findsym (real_c_register_pragma, "_Z17c_register_pragmaPKcS0_PFvP10cpp_readerE");
  findsym (real_cpp_define, "_Z10cpp_defineP10cpp_readerPKc");
  findsym (real_strip_typedefs, "_Z14strip_typedefsP9tree_nodePbj");
  findsym (real_lookup_base,
#if GCC_VERSION >= 14000
                                           "_Z11lookup_baseP9tree_nodeS0_iP9base_kindil"
#else
                                           "_Z11lookup_baseP9tree_nodeS0_iP9base_kindi"
#endif
            );
  findsym (real_type_as_string, "_Z14type_as_stringP9tree_nodei");
  findsym (real_decl_as_string, "_Z14decl_as_stringP9tree_nodei");
  findsym (real_skip_artificial_parms_for, "_Z25skip_artificial_parms_forPK9tree_nodePS_");
  findsym (real_dfs_walk_all, "_Z12dfs_walk_allP9tree_nodePFS0_S0_PvES3_S1_");
  findsym (real_cp_type_quals, "_Z13cp_type_qualsPK9tree_node");
  findsym (real_look_for_overrides_here, "_Z23look_for_overrides_hereP9tree_nodeS0_");

  return true;
}


} // namespace CheckerGccPlugins

