// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration.
// thread25_test: testing a regression seen with gcc13.

#pragma ATLAS check_thread_safety


struct IAuxTypeVectorFactory
{
  virtual ~IAuxTypeVectorFactory();
};


struct AuxTypeVectorFactory
  : public IAuxTypeVectorFactory
{
};


void foo (const IAuxTypeVectorFactory* p)
{
  new AuxTypeVectorFactory;
  delete p;
}


struct ParametersBase
{
  virtual ~ParametersBase() = default;
};
using TrackParameters = ParametersBase;


struct MuonPatternCombination {
  ~MuonPatternCombination();
  const TrackParameters* m_parameter;
};

MuonPatternCombination::~MuonPatternCombination() { delete m_parameter; }


const int* bar1();
void bar()
{
  delete bar1();
}
