// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
// testing check_discarded_const_from_return

#pragma ATLAS check_thread_safety

const int* xx();


int* f1()
{
  return const_cast<int*>(xx());
}


int* f2()
{
  int* y = const_cast<int*>(xx());
  return y;
}


int* f3 [[ATLAS::not_thread_safe]] ()
{
  int* y = const_cast<int*>(xx());
  return y;
}


int* f4 [[ATLAS::not_const_thread_safe]] ()
{
  int* y = const_cast<int*>(xx());
  return y;
}


int* f5 [[ATLAS::argument_not_const_thread_safe]] ()
{
  int* y = const_cast<int*>(xx());
  return y;
}

int* f6()
{
  int* y [[ATLAS::thread_safe]] = const_cast<int*>(xx());
  return y;
}


