# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Package building GeoModel for ATLAS.
#

# The name of the package:
atlas_subdir( GeoModel )

# External dependencies.
find_package( SQLite3 )
if( NOT ATLAS_BUILD_NLOHMANN_JSON )
   find_package( nlohmann_json )
endif()
find_package( ZLIB )

# Declare where to get GeoModel from.
set( ATLAS_GEOMODEL_SOURCE
   "URL;https://gitlab.cern.ch/GeoModelDev/GeoModel/-/archive/6.10.0/GeoModel-6.10.0.tar.bz2;URL_MD5;b5c2368db049c1a8dee223789cca03c0"
   CACHE STRING "The source for GeoModel" )
mark_as_advanced( ATLAS_GEOMODEL_SOURCE )

# Decide whether / how to patch the GeoModel sources.
set( ATLAS_GEOMODEL_PATCH
     ""
     CACHE STRING "Patch command for GeoModel" )
set( ATLAS_GEOMODEL_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of GeoModel (2024.3.12.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_GEOMODEL_PATCH ATLAS_GEOMODEL_FORCEDOWNLOAD_MESSAGE )

# Directory for the temporary build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/GeoModelBuild" )

# Extra configuration parameters.
set( _extraOptions )
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
    list( APPEND _extraOptions -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()
if( "${CMAKE_CXX_STANDARD}" GREATER_EQUAL 14 )
   list( APPEND _extraOptions -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()

# List of paths given to CMAKE_PREFIX_PATH.
set( _prefixPaths ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
    $ENV{CMAKE_PREFIX_PATH} ${SQLITE_LCGROOT} ${ZLIB_LCGROOT}
    ${nlohmann_json_DIR} )
if( ( NOT ATLAS_BUILD_EIGEN ) AND EIGEN_LCGROOT )
   find_package( Eigen )
   list( APPEND _prefixPaths ${EIGEN_LCGROOT} )
endif()
if( ( NOT ATLAS_BUILD_XERCESC ) AND XERCESC_LCGROOT )
   find_package( XercesC )
   list( APPEND _prefixPaths ${XERCESC_LCGROOT} )
endif()

# Set up the build of GeoModel:
ExternalProject_Add( GeoModel
  PREFIX "${CMAKE_BINARY_DIR}"
  INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
  ${ATLAS_GEOMODEL_SOURCE}
  ${ATLAS_GEOMODEL_PATCH}
  CMAKE_CACHE_ARGS
  -DCMAKE_PREFIX_PATH:PATH=${_prefixPaths}
  -DCMAKE_CXX_STANDARD_INCLUDE_DIRECTORIES:PATH=${SQLite3_INCLUDE_DIR}
  -DCMAKE_INSTALL_INCLUDEDIR:PATH=${CMAKE_INSTALL_INCLUDEDIR}/GeoModel
  -DCMAKE_INSTALL_LIBDIR:PATH=${CMAKE_INSTALL_LIBDIR}
  -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
  -DGEOMODEL_BUILD_TOOLS:BOOL=TRUE
  ${_extraOptions}
  LOG_CONFIGURE 1 )
ExternalProject_Add_Step( GeoModel forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_GEOMODEL_FORCEDOWNLOAD_MESSAGE}"
   INDEPENDENT TRUE
   DEPENDERS download )
ExternalProject_Add_Step( GeoModel purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for Gaudi"
   INDEPENDENT TRUE
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( GeoModel forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f "<BINARY_DIR>/CMakeCache.txt"
   COMMENT "Forcing the configuration of GeoModel"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( GeoModel buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing GeoModel into the build area"
   DEPENDEES install )

# add_dependencies
add_dependencies( GeoModel nlohmann_json )
if( ATLAS_BUILD_EIGEN )
   add_dependencies( GeoModel Eigen )
endif()
if( ATLAS_BUILD_XERCESC )
   add_dependencies( GeoModel XercesC )
endif()

# set "Package"
add_dependencies( Package_GeoModel GeoModel )

# Install GeoModel:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install its find-module:
install( FILES "cmake/FindGeoModel.cmake"
   DESTINATION "${CMAKE_INSTALL_CMAKEDIR}/modules" )
