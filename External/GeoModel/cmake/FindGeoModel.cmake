# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Locate the GeoModel external package.
#
# Defines:
#  GEOMODEL_FOUND
#  GEOMODEL_INCLUDE_DIR
#  GEOMODEL_INCLUDE_DIRS
#  GEOMODEL_<component>_FOUND
#  GEOMODEL_<component>_LIBRARY
#  GEOMODEL_LIBRARIES
#  GEOMODEL_LIBRARY_DIRS
#
# The user can set GEOMODEL_ATROOT to guide the script.
#

# Include the helper code:
include( AtlasInternals )

# Declare the module:
atlas_external_module( NAME GeoModel
   INCLUDE_SUFFIXES include include/GeoModel
   INCLUDE_NAMES GeoModelDBManager/GMDBManager.h
   		         GeoModelRead/ReadGeoModel.h
   		         GeoModelWrite/WriteGeoModel.h
   		         TFPersistification/ACosIO.h
   		         GeoModelKernel/GeoBox.h
                 GeoGenericFunctions/AbsFunction.h
                 GeoModelXMLParser/XMLHandler.h
                 ExpressionEvaluator/ExpressionEvaluator.h
   LIBRARY_SUFFIXES lib
   DEFAULT_COMPONENTS GeoModelKernel GeoGenericFunctions
                 GeoModelDBManager GeoModelRead GeoModelWrite
                 TFPersistification ExpressionEvaluator GeoModelJSONParser
                 GeoModelXMLParser GeoModelXml )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( GeoModel DEFAULT_MSG GEOMODEL_INCLUDE_DIR
   GEOMODEL_INCLUDE_DIRS GEOMODEL_LIBRARIES )
mark_as_advanced( GEOMODEL_FOUND GEOMODEL_INCLUDE_DIR GEOMODEL_INCLUDE_DIRS
   GEOMODEL_LIBRARIES GEOMODEL_LIBRARY_DIRS )
