VecMem
======

This package builds [vecmem](https://github.com/acts-project/vecmem) for
ATLAS's offline software.
