# Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
#
# Package building VecCore for the offline builds.
#

# The name of the package:
atlas_subdir( VecCore )

# Declare where to get VecCore from.
set( ATLAS_VECCORE_SOURCE
  "URL;https://lcgpackages.web.cern.ch/tarFiles/sources/VecCore-0.8.2.tar.gz;URL_MD5;1d0a4f4f64989b2277905c00ce88f120"
   CACHE STRING "The source for VecCore" )
mark_as_advanced( ATLAS_VECCORE_SOURCE )

# Decide whether / how to patch the VecCore sources.
set( ATLAS_VECCORE_PATCH
   ""
   CACHE STRING "Patch command for VecCore" )
set( ATLAS_VECCORE_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of VecCore (2025.02.10.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_VECCORE_PATCH ATLAS_VECCORE_FORCEDOWNLOAD_MESSAGE )

# Extra options for the configuration:
set( _extraOptions )

if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraOptions -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/VecCoreBuild )
# Directory holding the "stamp" files.
set( _stampDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/VecCoreStamp )

# Build VecCore for the build area:
ExternalProject_Add( VecCore
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   STAMP_DIR ${_stampDir}
   ${ATLAS_VECCORE_SOURCE}
   ${ATLAS_VECCORE_PATCH}
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCMAKE_INSTALL_LIBDIR:STRING=lib
   -DBUILD_TESTING:BOOL=FALSE
   ${_extraOptions}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( VecCore forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_VECCORE_FORCEDOWNLOAD_MESSAGE}"
   INDEPENDENT TRUE
   DEPENDERS download )
ExternalProject_Add_Step( VecCore purgebuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for VecCore"
   INDEPENDENT TRUE
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( VecCore forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f <BINARY_DIR>/CMakeCache.txt
   COMMENT "Forcing the configuration of VecCore"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( VecCore buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing VecCore into the build area"
   DEPENDEES install )
add_dependencies( Package_VecCore VecCore )


# Install VecCore:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
