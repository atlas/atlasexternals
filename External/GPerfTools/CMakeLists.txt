# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building/configuring gperftools as part of the offline
# software build.
#

# Set the name of the package:
atlas_subdir( GPerfTools )

# Set up a dependency on libunwind. Just to pull in its RPM for sure.
find_package( libunwind )
find_package( gperftools )

# Install the package's scripts:
atlas_install_scripts(
   scripts/atl-gpt-analyze
   scripts/atl-gpt-cpu-profile
   scripts/atl-gpt-mem-profile )

# Configure the environment setup module:
configure_file(
   "${CMAKE_CURRENT_SOURCE_DIR}/cmake/AtlasGPerfToolsEnvironmentConfig.cmake.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/AtlasGPerfToolsEnvironmentConfig.cmake"
   @ONLY )

# Set up the environment variable needed by the package:
if( GPERFTOOLS_FOUND )
   set( AtlasGPerfToolsEnvironment_DIR
      "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}"
      CACHE INTERNAL "Location of AtlasGPergToolsEnvironmentConfig.cmake" )
   find_package( AtlasGPerfToolsEnvironment )
endif()
