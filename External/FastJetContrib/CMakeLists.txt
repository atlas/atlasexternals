# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Package building the FastJet contrib libraries as part of the offline
# software build.
#

# Set the package name:
atlas_subdir( FastJetContrib )

# Declare where to get FastJetContrib from.
set( ATLAS_FASTJETCONTRIB_SOURCE
   "URL;https://cern.ch/lcgpackages/tarFiles/sources/fjcontrib-1.056.tar.gz;URL_MD5;d4474dba673134c6447befbeb3e3ced4"
   CACHE STRING "The source for FastJetContrib" )
mark_as_advanced( ATLAS_FASTJETCONTRIB_SOURCE )

# Decide whether / how to patch the FastJetContrib sources.
set( ATLAS_FASTJETCONTRIB_PATCH ""
   CACHE STRING "Patch command for FastJetContrib" )
set( ATLAS_FASTJETCONTRIB_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of FastJetContrib (2023.07.26.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_FASTJETCONTRIB_PATCH
   ATLAS_FASTJETCONTRIB_FORCEDOWNLOAD_MESSAGE )

# Temporary directory for the build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/FastJetContribBuild" )

# Decide where to pick up FastJet from.
if( ATLAS_BUILD_FASTJET )
   set( FASTJET_CONFIG_SCRIPT
      "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/fastjet-config" )
else()
   find_package( FastJet )
endif()

# Extra environment options for the configuration.
set( _cflags )

# Specify optimisation flags explicitly. But only do this for "known" compilers,
# not to intentionally break the build with more exotic ones.
if( ( "${CMAKE_CXX_COMPILER_ID}" MATCHES "GNU" ) OR
    ( "${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang" ) )
   if( "${CMAKE_BUILD_TYPE}" STREQUAL "Debug" )
      list( APPEND _cflags -O0 -g )
   else()
      list( APPEND _cflags -O2 )
   endif()
endif()

# Massage the options to make them usable in the configuration script.
string( REPLACE ";" " " _cflags "${_cflags}" )

# Set up the script used for configuring the build of FastJetConntrib.
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/cmake/configure.sh.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh" @ONLY )

# Set up the build of FastJetContrib for the build area:
ExternalProject_Add( FastJetContrib
   PREFIX "${CMAKE_BINARY_DIR}"
   ${ATLAS_FASTJETCONTRIB_SOURCE}
   ${ATLAS_FASTJETCONTRIB_PATCH}
   DOWNLOAD_EXTRACT_TIMESTAMP TRUE  # keep original timestamps (ATLINFR-4765)
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   CONFIGURE_COMMAND
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh"
   BUILD_IN_SOURCE 1
   BUILD_COMMAND
   "${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh" make
   COMMAND
   "${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh" make install
   INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory
   "${_buildDir}/" "<INSTALL_DIR>" )
ExternalProject_Add_Step( FastJetContrib forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo
   "${ATLAS_FASTJETCONTRIB_FORCEDOWNLOAD_MESSAGE}"
   INDEPENDENT TRUE
   DEPENDERS download )
if( ATLAS_BUILD_FASTJET )
   add_dependencies( FastJetContrib FastJet )
endif()
add_dependencies( Package_FastJetContrib FastJetContrib )

# Set up its installation:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
