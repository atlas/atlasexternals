# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Package building SQLite for ATLAS.
#

# The package's name.
atlas_subdir( SQLite )

# Declare where to get SQLite from.
set( ATLAS_SQLITE_SOURCE
   "URL;https://cern.ch/lcgpackages/tarFiles/sources/sqlite-autoconf-3320300.tar.gz;URL_MD5;2e3911a3c15e85c2f2d040154bbe5ce3"
   CACHE STRING "The source for SQLite" )
mark_as_advanced( ATLAS_SQLITE_SOURCE )

# Decide whether / how to patch the SQLite sources.
set( ATLAS_SQLITE_PATCH "" CACHE STRING "Patch command for SQLite" )
set( ATLAS_SQLITE_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of SQLite (2022.10.11.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_SQLITE_PATCH ATLAS_SQLITE_FORCEDOWNLOAD_MESSAGE )

# Temporary directory for the build results.
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/SQLiteBuild" )

# Extra build flag(s).
set( _extraFlags )
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Debug" )
   list( APPEND _extraFlags --enable-debug )
endif()

# Build SQLite.
ExternalProject_Add( SQLite
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_SQLITE_SOURCE}
   ${ATLAS_SQLITE_PATCH}
   DOWNLOAD_EXTRACT_TIMESTAMP TRUE  # keep original timestamps (ATLINFR-4765)
   CONFIGURE_COMMAND <SOURCE_DIR>/configure --prefix=${_buildDir}
   --enable-shared=yes --enable-static=false ${_extraFlags}
   CPPFLAGS=-DSQLITE_ENABLE_COLUMN_METADATA=1
   INSTALL_COMMAND make install
   COMMAND "${CMAKE_COMMAND}" -E copy_directory "${_buildDir}/" "<INSTALL_DIR>" )
ExternalProject_Add_Step( SQLite forcedownload
   COMMAND "${CMAKE_COMMAND}" -E echo "${ATLAS_SQLITE_FORCEDOWNLOAD_MESSAGE}"
   INDEPENDENT TRUE
   DEPENDERS download )
ExternalProject_Add_Step( SQLite purgeBuild
   COMMAND "${CMAKE_COMMAND}" -E remove_directory "<BINARY_DIR>"
   COMMAND "${CMAKE_COMMAND}" -E make_directory "<BINARY_DIR>"
   COMMAND "${CMAKE_COMMAND}" -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for SQLite"
   INDEPENDENT TRUE
   DEPENDEES download
   DEPENDERS patch )
add_dependencies( Package_SQLite SQLite )

# Install SQLite.
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
