# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Script installing the TBB files into a temporary directory from where they can
# be installed into the build directory and during the regular installation,
# easily.
#
# Heavily inspired by the lcgcmake implementation.
#

# Use the GNU install directories.
include( GNUInstallDirs )

# Find the compiled libraries.
file( GLOB release_libs
   ${SOURCE_DIR}/build/*_release/${CMAKE_SHARED_LIBRARY_PREFIX}*${CMAKE_SHARED_LIBRARY_SUFFIX}* )
file( GLOB debug_libs
   ${SOURCE_DIR}/build/*_debug/${CMAKE_SHARED_LIBRARY_PREFIX}*${CMAKE_SHARED_LIBRARY_SUFFIX}* )

# Install the libraries.
file( INSTALL ${release_libs}
   DESTINATION ${INSTALL_DIR}/${CMAKE_INSTALL_LIBDIR}
   USE_SOURCE_PERMISSIONS )
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Debug" OR
      "${CMAKE_BUILD_TYPE}" STREQUAL "RelWithDebInfo" )
   file( INSTALL ${debug_libs}
      DESTINATION ${INSTALL_DIR}/${CMAKE_INSTALL_LIBDIR}
      USE_SOURCE_PERMISSIONS )
endif()

# On PPC64 replace the weird library files made by TBB with simple soft links,
# as those seem to work much better on that platform.
if( "${CMAKE_SYSTEM_PROCESSOR}" MATCHES "ppc64" )

   # Get a list of the files that we link against.
   file( GLOB linked_libs
      ${INSTALL_DIR}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}*${CMAKE_SHARED_LIBRARY_SUFFIX} )

   # Remove these files.
   file( REMOVE ${linked_libs} )

   # Now get a list of the remaining files.
   file( GLOB libs
      ${INSTALL_DIR}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}* )

   # Create a soft link pointing at each of them.
   foreach( lib ${libs} )
      get_filename_component( libfile ${lib} NAME )
      get_filename_component( solibfile ${libfile} NAME_WE )
      set( solibfile "${solibfile}${CMAKE_SHARED_LIBRARY_SUFFIX}" )
      execute_process( COMMAND ${CMAKE_COMMAND} -E create_symlink
         "${libfile}" "${solibfile}"
         WORKING_DIRECTORY ${INSTALL_DIR}/${CMAKE_INSTALL_LIBDIR} )
   endforeach()
endif()

# Install the headers.
file( INSTALL ${SOURCE_DIR}/include/
   DESTINATION ${INSTALL_DIR}/${CMAKE_INSTALL_INCLUDEDIR}
   USE_SOURCE_PERMISSIONS )
