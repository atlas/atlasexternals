# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Package building KLFitter as part of an analysis release
#

# The name of the package:
atlas_subdir( KLFitter )

# Set ROOTSYS based on the build environment:
if( ATLAS_BUILD_ROOT )
   set( _rootsys "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}" )
else()
   find_package( ROOT REQUIRED )
   set( _rootsys "${ROOTSYS}" )
endif()

# Declare where to get KLFitter from.
set( ATLAS_KLFITTER_SOURCE
   "URL;http://cern.ch/atlas-software-dist-eos/externals/KLFitter/v1.4.0.tar.gz;URL_MD5;8bf9ad8b37a83fd9d5ae2d8ce9eb0f1e"
   CACHE STRING "The source for KLFitter" )
mark_as_advanced( ATLAS_KLFITTER_SOURCE )

# Decide whether / how to patch the KLFitter sources.
set( ATLAS_KLFITTER_PATCH "" CACHE STRING "Patch command for KLFitter" )
set( ATLAS_KLFITTER_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of KLFitter (2022.10.11.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_KLFITTER_PATCH ATLAS_KLFITTER_FORCEDOWNLOAD_MESSAGE )

# Temporary directory for the build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/KLFitterBuild" )

# Build KLFitter for the build area:
ExternalProject_Add( KLFitter
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_KLFITTER_SOURCE}
   ${ATLAS_KLFITTER_PATCH}
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DROOT_ROOT:PATH=${_rootsys}
   -DBAT_ROOT:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   -DINSTALL_EXAMPLES:BOOL=FALSE
   -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( KLFitter forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_KLFITTER_FORCEDOWNLOAD_MESSAGE}"
   INDEPENDENT TRUE
   DEPENDERS download )
ExternalProject_Add_Step( KLFitter purgeBuild
  COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
  COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
  COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
  COMMENT "Removing previous build results for KLFitter"
  INDEPENDENT TRUE
  DEPENDEES download
  DEPENDERS configure )
ExternalProject_Add_Step( KLFitter forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f "<BINARY_DIR>/CMakeCache.txt"
   COMMENT "Forcing the configuration of KLFitter"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( KLFitter buildinstall
  COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}/cmake"
  COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
  COMMENT "Installing KLFitter into the build area"
  DEPENDEES install )
add_dependencies( Package_KLFitter KLFitter )
add_dependencies( KLFitter BAT )
if( ATLAS_BUILD_ROOT )
   add_dependencies( KLFitter ROOT )
endif()

# Install KLFitter:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

install( FILES "cmake/FindKLFitter.cmake"
   DESTINATION "${CMAKE_INSTALL_CMAKEDIR}/modules" )
