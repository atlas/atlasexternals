ATLAS Externals Repository
==========================

This repository holds the code for building all flavours of ATLAS
"externals" projects. Projects holding code not developed by ATLAS,
but used by the offline/simulation/analysis software of the
experiment.

Code In The Repository
----------------------

The main project configurations can be found in the `Projects` directory.
You need to point CMake to one of those folders to build one of the
specific externals projects.

Other directories hold packages that either build, or set up some external
software to be used by downstream projects.

Build / Install
---------------

The projects can be built in a fairly standard way. The `AnalysisBaseExternals`
project should be possible to build on practically any POSIX platform that
supports [C++20](https://en.cppreference.com/w/cpp/20). All the other projects
on the other hand need to be built on some platform for which an
[LCG release](https://lcginfo.cern.ch/) is available.

You are highly recommended to use an out-of-source build for all projects,
even though in-source builds may also work. But they are generally not
supported/tested.

The most common build procedure is:

```bash
asetup ...
cmake -DCMAKE_BUILD_TYPE=Release -DCTEST_USE_LAUNCHERS=TRUE \
   -S atlasexternals/Projects/AthenaExternals -B build
cmake --build build/
DESTDIR=/some/location cmake --install build/
```

**N.B.**
- At the time of writing our production platform is `x86_64-el9-gcc13`, so you
  would execute the following to get a functional build environment

```
asetup none,gcc13,cmakesetup --cmakeversion=3.30.5
```

- You may need to ask for a specific CMake version explicitly. At the time of
  writing, you would need to use
  [CMake 3.30.5](https://github.com/Kitware/CMake/releases/tag/v3.30.5);
- See [AtlasSetupReference](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/AtlasSetupReference)
  for a lot more details about `asetup`.


Version
-------

This branch of the repository is meant to serve as a basis for the builds of
the various projects of the [atlas/athena](https://gitlab.cern.ch/atlas/athena)
repository's main branch.
